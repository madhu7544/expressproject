const { PrismaClient } = require("@prisma/client");
const yup = require("yup");
const prisma = new PrismaClient();

const todoSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean(),
});

const idSchema = yup.number().nullable().integer().positive();

const getTodos = async (req, res, next) => {
  try {
    const todos = await prisma.todo.findMany();
    return res.status(200).json(todos);
  } catch (err) {
    next(err);
  }
};

const getTodoById = async (req, res, next) => {
  try {
    const id = await idSchema.validate(parseInt(req.params.id));
    
    const todo = await prisma.todo.findUnique({
      where: { id: id },
    });
    if (todo) {
      return res.status(200).json(todo);
    } else {
      return res.status(404).json({ message: "Todo Not found" });
    }
  } catch (error) {
    next(error);
  }
};

const addTodo = async (req, res, next) => {
  try {
    await todoSchema.validate(req.body);
  } catch (error) {
    next(error);
  }
  try {
    const newTodo = await prisma.todo.create({
      data: req.body,
    });
    return res.status(201).json(newTodo);
  } catch (error) {
    return res.status(400).json({ message: "Invalid Todo" });
  }
};

const updateTodo = async (req, res, next) => {
  try {
    await todoSchema.validate(req.body);
  } catch (error) {
    return res.status(400).json({ message: "Invalid Todo" });
  }
  try {
    const id = await idSchema.validate(parseInt(req.params.id));
    const todo = await prisma.todo.findUnique({
      where: { id: id },
    });
    if (todo) {
      const updateTodo = await prisma.todo.update({
        where: { id: parseInt(req.params.id) },
        data: req.body,
      });
      return res.status(200).json(updateTodo);
    } else {
      return res.status(404).json({ message: "Todo not found" });
    }
  } catch (error) {
    next(error);
  }
};

const deleteTodoById = async (req, res, next) => {
  try {
    await idSchema.validate(parseInt(req.params.id));
  } catch (error) {
    next(error);
  }
  try {
    const deletedTodo = await prisma.todo.delete({
      where: {
        id: parseInt(req.params.id),
      },
    });
    return res.status(200).json({ message: "Todo deleted successfully" });
  } catch (error) {
    return res.status(404).json({ message: "Todo not found" });
  }
};

module.exports = {
  getTodos,
  getTodoById,
  addTodo,
  updateTodo,
  deleteTodoById,
};
