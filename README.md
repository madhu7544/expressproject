# Express CRUD Project

## Development Instructions

**1. Clone the repository_url**
```
git clone <repository_url>

```

**2. Install Dependencies**

```
npm install

```
**3. Create Database and user**

Open a Postgres terminal and run the following commands:
```
CREATE DATABASE todo
CREATE TABLE todo(
    id SERIAL PRIMARY KEY,
    text VARCHAR(300),
    iscompleted BOOLEAN DEFAULT false
)
```
**4. Set Evronment for Database**

Like in .env_example file you can put in the place of
DATABASE_URL="postgresql://user:password@host:/database?schema=public"
```
user:Your_username,
password:you_password,
host : localhost,
database:table_name

```

**5. Getting Database**

Pull the database by using terminal Run these commands

```
npx prisma migrate
npx prisma db pull
npx prisma generate
```

**6. Run the server**
```
node server.js

```

**7. By using post man you can do the following CRUD operations:**

```
GET --- "/todos"
GET By Id ---"/todos/:id"
POST --- "/todos"
PUT --- "/todos/:id"
DELETE --- "/todos/:id"
like these in your browser according to your localhost port or use postman