const express = require("express");
const todoRoutes = require("./src/todo/routes");
const errorHandler = require("./src/todo/errorHandler");

const app = express();
const port = 5000;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("You are in Right Path!");
});

app.use("/todo", todoRoutes);
app.use(errorHandler);

app.listen(port, () => {
  console.log(port);
});
